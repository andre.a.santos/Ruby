import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uby_controller/ui/bluetooth_page.dart';
import 'package:uby_controller/ui/internet_page.dart';
import 'package:uby_controller/ui/main_page.dart';


//void main() => runApp(MyApp());
String rota;

void main() async{
    WidgetsFlutterBinding.ensureInitialized();
    SharedPreferences.getInstance().then((prefs) {
      rota = prefs.get("rota").toString();
       runApp(new MyApp());
    });
     //runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'UBY CONTROLLER',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.red[900],
      ),
      //home: (rota!='internet'||rota!='bluetooth') ? MyHomePage(): rota == 'internet'? InternetPage(): rota =='bluetooth'? BluetoothPage(): MyHomePage(),
      home: rota =="internet"? InternetPage(): rota == "bluetooth"? BluetoothPage(): MyHomePage(), 
    );
  }
}
