import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uby_controller/ui/bluetooth_page.dart';
import 'package:uby_controller/ui/internet_page.dart';

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}
  
class _MyHomePageState extends State<MyHomePage>{
  
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void initState(){
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text('Ruby', style: TextStyle(fontSize:25),),),
      body: GridView.count(crossAxisCount: 2,
        children: <Widget>[
            GestureDetector(
              child: Container(
                color: Colors.red[700],
                child: Center(
                    child: Padding(
                  padding:
                      EdgeInsets.all(MediaQuery.of(context).size.width * 0.09),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(FontAwesomeIcons.bluetoothB,
                          color: Colors.white),
                      Text('Bluetooth',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: MediaQuery.of(context).size.width * 0.04
                        )
                      )
                    ],
                  ),
                ))
              ),
              onTap: () {
               Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                    BluetoothPage()
                  ), (Route<dynamic> route) => false
                );
              },
            ),
            GestureDetector(
              child: Container(
                color: Colors.white,
                child: Center(
                    child: Padding(
                  padding:
                      EdgeInsets.all(MediaQuery.of(context).size.width * 0.09),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(FontAwesomeIcons.wifi,
                          color: Colors.red[700]),
                      Text('Internet',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.red[700],
                            fontSize: MediaQuery.of(context).size.width * 0.04
                        )
                      )
                    ],
                  ),
                ))
              ),
              onTap: () {
               Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                    InternetPage()
                  ), (Route<dynamic> route) => false
                );
              },
            ),
        ]
      )
    );
  }
}


