import 'dart:convert';
import 'dart:typed_data';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BluetoothPage extends StatefulWidget {
  BluetoothDevice server;
  @override
  _BluetoothPageState createState() => _BluetoothPageState();
  }


class _Message {
  int whom;
  String text;

  _Message(this.whom, this.text);
}

class _BluetoothPageState extends State<BluetoothPage> {
  BluetoothConnection connection;
  bool isConnecting = true;
  bool isDisconnecting = false;
  bool get isConnected => connection != null && connection.isConnected;
  bool _valueTodas = false;
  bool _valuePulpito = false;
  bool _valueFrente = false;
  bool _valueFundo = false;
  bool _valueCentro = false;
  bool _valueLateral = false;
  bool _valueSobe = false;
  bool _valueDesce = false;

  bool result = false;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool pronto = false;

  final TextEditingController textEditingController =
      new TextEditingController();
  List<_Message> messages = List<_Message>();

  setRota() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('rota', "bluetooth");
    }

  @override
  void initState() {
    setRota();
    conectaBluetooth();
    conectaBluetoothAuto();
    
    super.initState();

    setState(() {});
  }

  void _onChangedTodas(bool value) {
    _valueTodas = value;

    if (value == true) {
      _onChangedPulpito(true);
      _onChangedCentro(true);
      _onChangedFrente(true);
      _onChangedFundo(true);
      _onChangedLateral(true);
    } else {
      _onChangedPulpito(false);
      _onChangedCentro(false);
      _onChangedFrente(false);
      _onChangedFundo(false);
      _onChangedLateral(false);
    }
    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"todas": value});
    setState(() {});
  }

  void _onChangedPulpito(bool value) {
    _valuePulpito = value;

   if (_valuePulpito == true) {
      _sendMessage('|D51|');
    } else if (_valuePulpito == false) {
      _sendMessage('|D50|');
    }

    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"pulpito": value});

    setState(() {});
  }

  void _onChangedFrente(bool value) {
    _valueFrente = value;

    if (_valueFrente == true) {
      _sendMessage('|D21|');
    } else {
      _sendMessage('|D20|');
    }
    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"frente": value});
    setState(() {});
  }

  void _onChangedFundo(bool value) {
    _valueFundo = value;

    if (_valueFundo == true) {
      _sendMessage('|D11|');
    } else {
      _sendMessage('|D10|');
    }
    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"fundo": value});
    setState(() {});
  }

  void _onChangedCentro(bool value) {
    _valueCentro = value;

    if (_valueCentro == true) {
      _sendMessage('|D41|');
    } else if (_valueCentro == false) {
      _sendMessage('|D40|');
    }
    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"centro": value});
    setState(() {});
  }

  void _onChangedLateral(bool value) {
    _valueLateral = value;

    if (_valueLateral == true) {
      _sendMessage('|D31|');
    }
    if (_valueLateral == false) {
      _sendMessage('|D30|');
    }
    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"lateral": value});
    setState(() {});
  }

  void _onChangedSobe(bool value) {
    _valueSobe = value;

    if (_valueSobe == true) {
      _valueDesce = false;
    }

    if (_valueSobe == true) {
      _sendMessage('|P6|');
    }
   
    setState(() {});
  }

  void _onChangedDesce(bool value) {
    _valueDesce = value;

    if (_valueDesce == true) {
      _valueSobe = false;
    }

    if (_valueDesce == true) {
      _sendMessage('|P7|');
    }
  
    setState(() {});
  }

  Future<void> conectaBluetooth() async {
    try {
      var _connection = await BluetoothConnection.toAddress("98:D3:33:81:15:DC"); //cel
      //await BluetoothConnection.toAddress("00:18:E4:40:00:06"); //tablet

      connection = _connection;

      setState(() {
        isConnecting = true;
        isDisconnecting = false;
      });
      
      connection.input.listen((Uint8List data) {
        print('Data incoming: ${ascii.decode(data)}');
        connection.output.add(data); // Sending data

        if (ascii.decode(data).contains('!')) {
            connection.finish(); // Closing connection
            conectaBluetooth3();
        }
      }).onDone(() {
        print('Disconnected by remote request');
        conectaBluetooth3();
      });

      connection.input.listen(_onDataReceived).onDone(() {
        if (this.mounted) {
          setState(() {});
        }
      });
    } catch (e) {
      print(e);
      if (e.toString().contains("Exception")) {
        await showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Serviço Indisponível"),
                content: Text("Dispositivo Bluetooth não encontrado."),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Reconectar"),
                    onPressed: () async {
                      Navigator.pop(context);
                      await conectaBluetooth();
                    },
                  )
                ],
              );
            });
      }
    }
  }

  Future<void> conectaBluetooth3() async{
     try {
      var _connection = await BluetoothConnection.toAddress("98:D3:33:81:15:DC"); //cel
      //await BluetoothConnection.toAddress("00:18:E4:40:00:06"); //tablet

      connection = _connection;

      setState(() {
        isConnecting = true;
        isDisconnecting = false;
      });

      connection.input.listen((Uint8List data) {
        print('Data incoming: ${ascii.decode(data)}');
        connection.output.add(data); // Sending data

        if (ascii.decode(data).contains('!')) {
            connection.finish(); // Closing connection
            conectaBluetooth3();
        }
      }).onDone(() {
        print('Disconnected by remote request');
        conectaBluetooth3();
      });
    
      connection.input.listen(_onDataReceived).onDone(() {
        if (this.mounted) {
          setState(() {});
        }
      });
    } catch (e) {
      if (e.toString().contains("Exception")) {
        await conectaBluetooth3();
      }
    }

  }

  void conectaBluetoothAuto(){
    Future.delayed(const Duration(hours: 1), () {

      conectaBluetooth2();
      conectaBluetoothAuto();
    
    });
  }

  void conectaBluetooth2() {
    BluetoothConnection.toAddress("98:D3:33:81:15:DC").then((_connection) {
      //BluetoothConnection.toAddress("00:18:E4:40:00:06").then((_connection) {
      //tablet

      connection = _connection;

      setState(() {
        isConnecting = true;
        isDisconnecting = false;
      });

      connection.input.listen(_onDataReceived).onDone(() {
        if (this.mounted) {
          setState(() {});
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text(
            "Ruby",
            style: TextStyle(fontSize: 25.0),
          ),
           actions: <Widget>[
            IconButton(icon: Icon(Icons.bluetooth), onPressed: conectaBluetooth2)
          ],
        ),
        body: StreamBuilder(
          stream: Firestore.instance
              .document("botoes/EgWDQsKa2YY0HyknDfo3")
              .snapshots(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                break;
              case ConnectionState.waiting:
                break;
              default:
                if (_valueCentro == true) {
                  _sendMessage('|D41|');
                }
                if (_valueCentro == false) {
                  _sendMessage('|D40|');
                }
                if (_valuePulpito == true) {
                  _sendMessage('|D51|');
                }
                if (_valuePulpito == false) {
                  _sendMessage('|D50|');
                }
                if (_valueFrente == true) {
                  _sendMessage('|D21|');
                } 
                if (_valueFrente == false) {
                  _sendMessage('|D20|');
                }
                if (_valueFundo == true) {
                  _sendMessage('|D11|');
                } 
                if (_valueFundo == false) {
                  _sendMessage('|D10|');
                }
                if (_valueLateral == true) {
                  _sendMessage('|D31|');
                }
                if (_valueLateral == false) {
                  _sendMessage('|D30|');
                }

                _valueTodas = snapshot.data.data['todas'];
                _valuePulpito = snapshot.data.data['pulpito'];
                _valueFrente = snapshot.data.data['frente'];
                _valueFundo = snapshot.data.data['fundo'];
                _valueCentro = snapshot.data.data['centro'];
                _valueLateral = snapshot.data.data['lateral'];
                _valueSobe = snapshot.data.data['sobe'];
                _valueDesce = snapshot.data.data['desce'];

            }
            return ListView(
              padding: const EdgeInsets.all(0),
              children: <Widget>[
                Container(
                  padding: new EdgeInsets.all(
                      MediaQuery.of(context).size.width * 0.035),
                  child: new Column(children: <Widget>[
                    new SwitchListTile(
                      title: new Text(
                        'LUZ TODAS',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 30.0),
                      ),
                      activeColor: Colors.white,
                      activeTrackColor: Colors.green,
                      value: _valueTodas,
                      onChanged: (bool value) {
                        _onChangedTodas(value);
                      },
                    )
                  ]),
                  color: Colors.white,
                ),
                Container(
                  padding: new EdgeInsets.all(
                      MediaQuery.of(context).size.width * 0.035),
                  child: new Column(
                    children: <Widget>[
                      new SwitchListTile(
                        title: new Text(
                          'LUZ PÚLPITO',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0),
                        ),
                        activeColor: Colors.white,
                        activeTrackColor: Colors.green,
                        value: _valuePulpito,
                        onChanged: (bool value) {
                          _onChangedPulpito(value);
                        },
                      ),
                    ],
                  ),
                  color: Colors.red[700],
                ),
                Container(
                  padding: new EdgeInsets.all(
                      MediaQuery.of(context).size.width * 0.035),
                  child: new Column(children: <Widget>[
                    new SwitchListTile(
                      title: new Text(
                        'LUZ FRENTE',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 30.0),
                      ),
                      activeColor: Colors.white,
                      activeTrackColor: Colors.green,
                      value: _valueFrente,
                      onChanged: (bool value) {
                        _onChangedFrente(value);
                      },
                    )
                  ]),
                  color: Colors.white,
                ),
                Container(
                  padding: new EdgeInsets.all(
                      MediaQuery.of(context).size.width * 0.035),
                  child: new Column(
                    children: <Widget>[
                      new SwitchListTile(
                        title: new Text(
                          'LUZ FUNDO',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0),
                        ),
                        activeColor: Colors.white,
                        activeTrackColor: Colors.green,
                        value: _valueFundo,
                        onChanged: (bool value) {
                          _onChangedFundo(value);
                        },
                      ),
                    ],
                  ),
                  color: Colors.red[700],
                ),
                Container(
                  padding: new EdgeInsets.all(
                      MediaQuery.of(context).size.width * 0.035),
                  child: new Column(
                    children: <Widget>[
                      new SwitchListTile(
                        title: new Text(
                          'LUZ CENTRO',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0),
                        ),
                        activeColor: Colors.white,
                        activeTrackColor: Colors.green,
                        value: _valueCentro,
                        onChanged: (bool value) {
                          _onChangedCentro(value);
                        },
                      ),
                    ],
                  ),
                  color: Colors.white,
                ),
                Container(
                  padding: new EdgeInsets.all(
                      MediaQuery.of(context).size.width * 0.035),
                  child: new Column(
                    children: <Widget>[
                      new SwitchListTile(
                        title: new Text(
                          'LUZ LATERAL',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0),
                        ),
                        activeColor: Colors.white,
                        activeTrackColor: Colors.green,
                        value: _valueLateral,
                        onChanged: (bool value) {
                          _onChangedLateral(value);
                        },
                      ),
                    ],
                  ),
                  color: Colors.red[700],
                ),
                Container(
                  padding: new EdgeInsets.all(
                      MediaQuery.of(context).size.width * 0.035),
                  child: new Column(
                    children: <Widget>[
                      new SwitchListTile(
                        title: new Text(
                          'CORTINA SOBE',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0),
                        ),
                        activeColor: Colors.white,
                        activeTrackColor: Colors.green,
                        value: _valueSobe,
                        onChanged: (bool value) {
                          _onChangedSobe(value);
                        },
                      ),
                    ],
                  ),
                  color: Colors.white,
                ),
                Container(
                  padding: new EdgeInsets.all(
                      MediaQuery.of(context).size.width * 0.035),
                  child: new Column(
                    children: <Widget>[
                      new SwitchListTile(
                        title: new Text(
                          'CORTINA DESCE',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0),
                        ),
                        activeColor: Colors.white,
                        activeTrackColor: Colors.green,
                        value: _valueDesce,
                        onChanged: (bool value) {
                          _onChangedDesce(value);
                        },
                      ),
                    ],
                  ),
                  color: Colors.red[700],
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  void _onDataReceived(Uint8List data) {
    int backspacesCounter = 0;
    data.forEach((byte) {
      if (byte == 8 || byte == 127) {
        backspacesCounter++;
      }
    });
    Uint8List buffer = Uint8List(data.length - backspacesCounter);
    int bufferIndex = buffer.length;

    backspacesCounter = 0;
    for (int i = data.length - 1; i >= 0; i--) {
      if (data[i] == 8 || data[i] == 127) {
        backspacesCounter++;
      } else {
        if (backspacesCounter > 0) {
          backspacesCounter--;
        } else {
          buffer[--bufferIndex] = data[i];
        }
      }
    }
  }

   void _sendMessage(String text) async {
    textEditingController.clear();

    if (text.length > 0) {
      try {
        connection.output.add(utf8.encode(text));
        await connection.output.allSent;

        setState(() {});

        Future.delayed(Duration(milliseconds: 333)).then((_) {});
      } catch (e) {
      }
        
      }
    }
  }

