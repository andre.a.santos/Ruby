import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:ui';

import 'package:shared_preferences/shared_preferences.dart';


class InternetPage extends StatefulWidget {

  _InternetPageState createState() => _InternetPageState();
}


class _InternetPageState extends State<InternetPage> {

  bool _valueTodas = false;
  bool _valuePulpito = false;
  bool _valueFrente = false;
  bool _valueFundo = false;
  bool _valueCentro = false;
  bool _valueLateral = false;

  bool conectado;
  bool popupAberto;
  StreamSubscription<ConnectivityResult> subscription;
  bool result = false;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool pronto = false;

  final TextEditingController textEditingController =
      new TextEditingController();

  setRota() async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('rota', "internet");
  }

  @override
  void initState() {
    setRota();
    super.initState();
    
    subscription = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult connectivityResult) {
        if (connectivityResult == ConnectivityResult.mobile) {
          if(popupAberto == true){
            Navigator.pop(context);
            popupAberto = false;
          }
          conectado = true;
        } else if (connectivityResult == ConnectivityResult.wifi) {
          if(popupAberto == true){
            Navigator.pop(context);
            popupAberto = false;
          }
          conectado = true;
        } else {
          conectado = false;
          conectaInternet();
          popupAberto = true;
        }
      });

    setState(() {});
  }

  

  void _onChangedTodas(bool value) {
    _valueTodas = value;

    if (value == true) {
      _onChangedPulpito(true);
      _onChangedCentro(true);
      _onChangedFrente(true);
      _onChangedFundo(true);
      _onChangedLateral(true);
    } else {
      _onChangedPulpito(false);
      _onChangedCentro(false);
      _onChangedFrente(false);
      _onChangedFundo(false);
      _onChangedLateral(false);
    }
    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"todas": value});
    setState(() {});
  }

  void _onChangedPulpito(bool value) {
    _valuePulpito = value;

    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"pulpito": value});
    setState(() {});
  }

  void _onChangedFrente(bool value) {
    _valueFrente = value;

    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"frente": value});
    setState(() {});
  }

  void _onChangedFundo(bool value) {
    _valueFundo = value;

    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"fundo": value});
    setState(() {});
  }

  void _onChangedCentro(bool value) {
    _valueCentro = value;

    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"centro": value});
    setState(() {});
  }

  void _onChangedLateral(bool value) {
    _valueLateral = value;

    Firestore.instance
        .document("botoes/EgWDQsKa2YY0HyknDfo3")
        .updateData({"lateral": value});
    setState(() {});
  }

  Future<void> conectaInternet() async {
    await showDialog(
              context: context,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return WillPopScope(
                  onWillPop: () async {},
                  child: AlertDialog(
                   title: Text("Serviço Indisponível"),
                    content: Text("Sem acesso a internet."),
                    ), 
                  );
                
              });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(
          "Ruby",
          style: TextStyle(fontSize: 25.0),
        ),
       
      ),
      body: StreamBuilder(
        stream: Firestore.instance
            .document("botoes/EgWDQsKa2YY0HyknDfo3")
            .snapshots(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
              break;
            case ConnectionState.waiting:
              break;
            default:
              
              _valueTodas = snapshot.data.data['todas'];
              _valuePulpito = snapshot.data.data['pulpito'];
              _valueFrente = snapshot.data.data['frente'];
              _valueFundo = snapshot.data.data['fundo'];
              _valueCentro = snapshot.data.data['centro'];
              _valueLateral = snapshot.data.data['lateral'];

          }
          return ListView(
            padding: const EdgeInsets.all(0),
            children: <Widget>[
              Container(
                padding: new EdgeInsets.all(
                    MediaQuery.of(context).size.width * 0.05),
                child: new SwitchListTile(
                  title: new Text(
                    'LUZ TODAS',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 30.0),
                  ),
                  activeColor: Colors.white,
                  activeTrackColor: Colors.green,
                  value: _valueTodas,
                  onChanged: (bool value) {
                    _onChangedTodas(value);
                  },
                ),
                color: Colors.white,
              ),
              Container(
                padding: new EdgeInsets.all(
                    MediaQuery.of(context).size.width * 0.05),
                child: new Column(
                  children: <Widget>[
                    new SwitchListTile(
                      title: new Text(
                        'LUZ PÚLPITO',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 30.0),
                      ),
                      activeColor: Colors.white,
                      activeTrackColor: Colors.green,
                      value: _valuePulpito,
                      onChanged: (bool value) {
                        _onChangedPulpito(value);
                      },
                    ),
                  ],
                ),
                color: Colors.red[700],
              ),
              Container(
                padding: new EdgeInsets.all(
                    MediaQuery.of(context).size.width * 0.05),
                child: new SwitchListTile(
                  title: new Text(
                    'LUZ FRENTE',
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 30.0),
                  ),
                  activeColor: Colors.white,
                  activeTrackColor: Colors.green,
                  value: _valueFrente,
                  onChanged: (bool value) {
                    _onChangedFrente(value);
                  },
                ),
                color: Colors.white,
              ),
              Container(
                padding: new EdgeInsets.all(
                    MediaQuery.of(context).size.width * 0.05),
                child: new Column(
                  children: <Widget>[
                    new SwitchListTile(
                      title: new Text(
                        'LUZ FUNDO',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 30.0),
                      ),
                      activeColor: Colors.white,
                      activeTrackColor: Colors.green,
                      value: _valueFundo,
                      onChanged: (bool value) {
                        _onChangedFundo(value);
                      },
                    ),
                  ],
                ),
                color: Colors.red[700],
              ),
              Container(
                padding: new EdgeInsets.all(
                    MediaQuery.of(context).size.width * 0.05),
                child: new Column(
                  children: <Widget>[
                    new SwitchListTile(
                      title: new Text(
                        'LUZ CENTRO',
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 30.0),
                      ),
                      activeColor: Colors.white,
                      activeTrackColor: Colors.green,
                      value: _valueCentro,
                      onChanged: (bool value) {
                        _onChangedCentro(value);
                      },
                    ),
                  ],
                ),
                color: Colors.white,
              ),
              Container(
                padding: new EdgeInsets.all(
                    MediaQuery.of(context).size.width * 0.05),
                child: new Column(
                  children: <Widget>[
                    new SwitchListTile(
                      title: new Text(
                        'LUZ LATERAL',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 30.0),
                      ),
                      activeColor: Colors.white,
                      activeTrackColor: Colors.green,
                      value: _valueLateral,
                      onChanged: (bool value) {
                        _onChangedLateral(value);
                      },
                    ),
                  ],
                ),
                color: Colors.red[700],
              ),
            ],
          );
        },
      ),
    );
  }

}
